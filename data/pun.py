import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import pandas as pd
import numpy as np
import dash_core_components as dcc
import plotly.express as px
import plotly.figure_factory as ff
from dash.dependencies import Input, Output, State

app = dash.Dash(external_stylesheets=[dbc.themes.FLATLY])
df = pd.read_excel('C:\\Users\\firhana\\project30\\project\\data\\data_dropout_59-64.xlsx')
app.layout = html.Div([
    html.Div([
    # html.Br(),
        html.H1('สาขา',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
        dcc.Dropdown(
            options=[{'label': col, 'value': col} for col in ['0200','0203','0204','0205','0210','0214','0215','0216','0220','0225','0226','0230','0235','0240','0250','0254']], 
            id='MAJOR_ID',
            style={'width': '150px',
                   'marginRight' : '20px'} , 
            placeholder='สาขา'
        )
    ], style={'display':'inline-block'}),
    
        html.Div([
            html.H1('ภาควิชา',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['028','029','030','031','032','034','193']], 
                id='DEPT_ID',
                style={'width': '150px',
                    'marginRight' : '20px'} ,
                placeholder='ภาควิชา'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1('ประเภทการศึกษา',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['01','04','07']], 
                id='STUDY_TYPE_ID',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='ประเภทการศึกษา'
            )
        ], style={'display':'inline-block'}),


        html.Div([
            html.H1('เพศ',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['0','1']], 
                id='SEX_SHORT_NAME_THAI',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='เพศ'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1('ระบบที่ได้เข้าการศึกษา',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['00','01','02','03','04','10','18','25',
                                                                  '33','36','38','39','40','46','48','54',
                                                                  '60','61','71','77','78','81','84',
                                                                  '89','B1','D0','E1','P4']], 
                id='ENT_METHOD',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='ระบบที่ได้เข้าการศึกษา'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1('ทุนการศึกษา',style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['0','1']], 
                id='FUND_NAME_CODE',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='ทุนการศึกษา'
            )
        ], style={'display':'inline-block'}),


    html.Div(id='pandas-output-container-1')
])


@app.callback(
    Output('pandas-output-container-1', 'children'),
    Input('MAJOR_ID', 'value'),
    Input('DEPT_ID', 'value'),
    Input('STUDY_TYPE_ID','value'),
    Input('SEX_SHORT_NAME_THAI','value'),
    Input('ENT_METHOD','value'),
    Input('FUND_NAME_CODE','value'),
    

)
def update_output(value1, value2, value3,value4,value5,value6):
    return f'You have selected {value1} and {value2} and {value3} and {value4} and {value5} and {value6}.'

@app.callback(
        Output('Scatter-chart', 'figure'),
              Output('Scatter-title', 'children'),
              Input('my-button', 'n_clicks'),
              State(major, 'value'))
def update_bar(n_clicks,project_ent):
    scatter_df = df.groupby('ENT_METHOD_DESC').count().loc[['Admission','โครงการ "โควตาวิศวกรรมศาสตร์ มอ."','โครงการ "ทุนมงคลสุข"','โครงการ "ลูกพระราชบิดา "','โครงการการคัดเลือกบุคคลเข้าศึกษาระดับปริญญาตรี รอบที่ 3 รับตรงกลางกัน (Admission 2)','โครงการความร่วมมือกับมูลนิธิส่งเสริมโอลิมปิกวิชาการและพัฒนามาตรฐานวิทยาศาสตร์ในพระอุปถัมภ์ (สอวน.)',
                                           'โครงการคัดเลือกนักเรียนเข้าศึกษาในคณะวิศวกรรมศาสตร์ สาขาวิชาวิศวกรรมและการจัดการนวัตกรรม ',
                                           'โครงการคัดเลือกนักเรียนใน 14  จังหวัดภาคใต้ เข้าศึกษาโดยวิธีรับตรง (โควตาภูมิภาค)','โครงการคัดเลือกบุคคลเข้าศึกษาโดยใช้ผลคะแนนสอบ GAT/PAT และวิชาสามัญ 9 วิชา','โครงการคัดเลือกผู้มีความสามารถด้านกีฬา','โครงการดาวรุ่งคอมพิวเตอร์','โครงการรับนักเรียนที่มีผลการเรียนดี',
                                           'โครงการส่งเสริมผู้มีคุณธรรม จริยธรรม บำเพ็ญประโยชน์ช่วยเหลือสังคม','ทุนอุดมศึกษาจังหวัดชายแดนภาคใต้(ศอ.บต.)'],"GPA_SCHOOL"]

    fig_scatter = px.scatter(scatter_df,
                     x=project_ent,
                     y=scatter_df,
                     color='GPA_SCHOOL',
                     color_discrete_sequence=['#bad6eb', '#2b7bba'])

    fig_scatter.update_layout(
        width=500,
        height=340,
        title_text = 'โครงการที่ได้รับเข้าเลือกศึกษา',
        title_x = 1,
        title_y = 1.02
        )
if __name__ == "__main__":
    app.run_server(debug=True, port=5010)