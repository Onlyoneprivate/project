from dash import dcc, Input, Output, State, html
import dash
import dash_bootstrap_components as dbc
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.figure_factory as ff
from pycaret.classification import *

app = dash.Dash(external_stylesheets=[dbc.themes.FLATLY])
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Clean data
df = pd.read_excel('data_dropout_59-64.xlsx')
# major = df.groupby('MAJOR_NAME_THAI').count().loc[['วิศวกรรมเครื่องกล', 'วิศวกรรมอุตสาหการ', 'วิศวกรรมไฟฟ้า',
#        'วิศวกรรมเหมืองแร่','วิศวกรรมโยธา','วิศวกรรมเคมี',
#        'วิศวกรรมเมคาทรอนิกส์','วิศวกรรมวัสดุ','ยังไม่แยกสาขาวิชา',
#        'วิศวกรรมคอมพิวเตอร์','วิศวกรรมการผลิต','วิศวกรรมชีวการแพทย์',
#        'วิศวกรรมสิ่งแวดล้อม','วิศวกรรมและการจัดการนวัตกรรม',
#        'วิศวกรรมเหมืองแร่และวัสดุ','วิศวกรรมปัญญาประดิษฐ์'],'GPA_SCHOOL']

major = df['MAJOR_NAME_THAI'].unique().tolist()
dept =df['DEPT_NAME_THAI'].unique().tolist()

model_group10 = load_model('group10_model')
# prediction = predict_model(model_group10)
# #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#App layout
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
pie = df.groupby('SEX_SHORT_NAME_THAI').count().loc[["ช","ญ"],"GPA_SCHOOL"]
#pie chart
fig_pie = px.pie(pie.reset_index(),
                 values='GPA_SCHOOL',
                 names='SEX_SHORT_NAME_THAI',
                 color_discrete_sequence=['#bad6eb', '#2b7bba'])

fig_pie.update_layout(
    width=320,
    height=250,
    margin=dict(l=30, r=10, t=10, b=10),
    paper_bgcolor='rgba(0,0,0,0)',
)

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#sidebar

sidebar = html.Div(
    [
        dbc.Row(
            [
    
                html.H5('Data Dropout',
                        style={'margin-top': '12px', 'margin-left': '24px'})
                ],
            style={"height": "5vh"},
            className='bg-primary text-white font-italic'
            ),
        dbc.Row(
            [
                html.Div([
                    html.P('สาขา',
                        style={'margin-top': '8px', 'margin-bottom': '4px'},
                        className='font-weight-bold'),

                    dcc.Dropdown(major,id="major",
                        placeholder='เลือกสาขา',
                        style={'width': '320px'}
                        ),
                    html.P('ภาควิชา',
                           style={'margin-top': '16px', 'margin-bottom': '4px'},
                           className='font-weight-bold'),
                    dcc.Dropdown(dept,id ='dept',
                                placeholder='ภาควิชา',
                                style={'width': '320px'}
                                ),
                    html.P('ปีการศึกษา',
                           style={'margin-top': '16px', 'margin-bottom': '4px'},
                           className='font-weight-bold'),
                    dcc.Slider(
                        id='year-slider',
                        min=2559,
                        max=2564,
                        marks={i: f'{i}' for i in range(2559, 2565)}),
                    ], style={'padding': 10, 'flex': 1}),
                     html.Button(id='my-button', n_clicks=0, children='apply',
                                style={'margin-top': '16px'},
                                className='bg-dark text-white'),
                    html.Hr(),
                     dbc.Row(
            [
                html.Div([
                    html.P('Target Variables', className='font-weight-bold'),
                    dcc.Graph(figure=fig_pie)
                    ])
                ],
            style={"height": "45vh", 'margin': '8px'}
            )
                    ]
                    )
                ],
            style={'height': '50vh', 'margin': '8px'}),
       

    
content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div([
                            html.P(id='hist-title',
                                   className='font-weight-bold'),
                            dcc.Graph(id="hist-chart",
                                      className='bg-light')])
                        ]),
                dbc.Col(
                    [
                        html.Div([
                            html.P(id='area-title',
                                   className='font-weight-bold'),
                            dcc.Graph(id="area-chart",
                                      className='bg-light')])
                        ])
                ],
            style={'height': '50vh',
                   'margin-top': '16px', 'margin-left': '8px',
                   'margin-bottom': '8px', 'margin-right': '8px'}),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div([
                            html.P(id = 'out_p',
                                   className='font-weight-bold'),
                            ]),
                        
                
            ])
                ],
            style={'height': '50vh',
                   'margin-top': '50px', 'margin-left': '50px',
                   'margin-bottom': '50px', 'margin-right': '50px'})

    ])
        



@app.callback(Output('hist-chart', 'figure'),
              Output('hist-title', 'children'),
              Input('my-button', 'n_clicks'),
              State("major", 'value'))
def update_bar(people, major_ps):
    fig_hist = px.histogram(df['GPA_SCHOOL'],x=df['MAJOR_NAME_THAI'],color=df['MAJOR_NAME_THAI'])

    

    fig_hist.update_layout(
        width=500,
        height=340,
        margin=dict(l=40, r=20, t=20, b=30),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)',
        legend_title=None,
        yaxis_title=None,
        xaxis_title=None,
        legend=dict(
            orientation="h",
            yanchor="bottom",
            y=1.02,
            xanchor="right",
            x=1
        )
    )

    title_hist = 'People of Major ' 

    return fig_hist, title_hist


@app.callback(Output('area-chart', 'figure'),
              Output('area-title', 'children'),
              Input('my-button', 'n_clicks'),
              State('dept', 'value'))
def update_area(people, province):

    fig_area = px.area(df['GPA_SCHOOL'],x=df['PROVINCE_NAME_THAI'],color=df['PROVINCE_NAME_THAI'])

    fig_area.update_layout(width=500,
                           height=340,
                           margin=dict(t=20, b=20, l=40, r=20),
                           paper_bgcolor='rgba(0,0,0,0)',
                           plot_bgcolor='rgba(0,0,0,0)',
                           legend=dict(
                               orientation="h",
                               yanchor="bottom",
                               y=1.02,
                               xanchor="right",
                               x=1
                           ))

    title_area = 'Province'

    return fig_area, title_area
@app.callback(Output("out_p","children"),
            #   Output("predict-title", "children"),
              [Input('button','n_clicks')],
              [Input("input1","value"),
               Input("input2","value"),
               Input("input3","value"),
               Input("input4","value"),
               Input("input5","value"),
               Input("input6","value"),
               Input("input7","value"),
               Input("input8","value"),
            #    Input("input9","value"),
               Input("สาขา","value"),
               Input("รหัสภาควิชา","value"),
               Input("ภาควิชา","value"),
               Input("FAC_ID","value"),
               Input("FAC_NAME_THAI","value"),
               Input("STUDY_LEVEL_ID","value"),
               Input("STUDY_LEVEL_NAME","value"),
               Input("เพศ","value"),
               Input("โครงการ","value"),
               Input("STUDY_TYPE_ID","value"),
               Input("FUND_NAME_CODE","value")
               ])

def predictions(a1,b1,c1,d1,e1,f1,g1,h1,i1,j1,k1,l1,m1,n1,o1,p1,q1,r1,s1,t1):
    Dicts = {"เกรดปี1เทอม1":[a1],
             "เกรดปี1เทอม2":[b1],
             "เกรดปี2เทอม1":[c1],
             "เกรดปี2เทอม2":[d1],
             "เกรดปี3เทอม1":[e1],
             "เกรดปี3เทอม2":[f1],
             "เกรดปี4เทอม1":[g1],
             "เกรดปี4เทอม2":[h1],
             "ปีการศึกษา":[i1],
             "สาขา":[j1],
             "รหัสภาควิชา":[k1],
             "ภาควิชา":[l1],
             "FAC_ID":[m1],
             "FAC_NAME_THAI":[n1],
             "STUDY_LEVEL_ID":[o1],
             "STUDY_LEVEL_NAME":[p1],
             "เพศ":[q1],    
             "โครงการ":[r1],
             "STUDY_TYPE_ID":[s1],
             "FUND_NAME_CODE":[t1]

    }
    # s = {"G":"R","R":"G"}

    data = pd.DataFrame(Dicts)
    predictions = predict_model(model_group10,data)

    # fig_predict = px.pie(predictions,
    #                      values=[predictions["prediction_score"],1-predictions["prediction_score"]],
    #                      names=predictions,
    #                      color=[predictions["prediction_label"][0],s[predictions["prediction_label"][0]]])
    # fig_predict.update_layout(
    #     width=320,
    #     height=250,
    #     margin=dict(l=30, r=10, t=10, b=10),
    #     paper_bgcolor='rgba(0,0,0,0)',
    # )
    # title_predict = 'PREDICTION'

    return predictions["prediction_label"]

drop_down = html.Div([
    html.Div([
    # html.Br(),
        html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
        dcc.Dropdown(
            options=[{'label': col, 'value': col} for col in ['0200','0203','0204','0205','0210','0214','0215','0216','0220','0225','0226','0230','0235','0240','0250','0254']], 
            id='สาขา',
            style={'width': '150px',
                   'marginRight' : '20px'} , 
            placeholder='สาขา'
        )
    ], style={'display':'inline-block'}),
    
        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['028','029','030','031','032','034','193']], 
                id='รหัสภาควิชา',
                style={'width': '150px',
                    'marginRight' : '20px'} ,
                placeholder='ภาควิชา'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['01','04','07']], 
                id='ภาควิชา',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='ภาควิชา'
            )
        ], style={'display':'inline-block'}),


        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['0','1']], 
                id='SEX_SHORT_NAME_THAI',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='เพศ'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['06']], 
                id='FAC_ID',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='FAC_ID'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['01','04','07']], 
                id='STUDY_TYPE_ID',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='STUDY_TYPE_ID'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['N','Y','Other']], 
                id='FUND_NAME_CODE',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='FUND_NAME_CODE'
            )
        ], style={'display':'inline-block'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['คณะวิศวกรรมศาสตร์']], 
                id='FAC_NAME_THAI',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='FAC_NAME_THAI'
            )
        ], style={'display':'inline-block','margin-left':'8px'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['06']], 
                id='STUDY_LEVEL_ID',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='STUDY_LEVEL_ID'
            )
        ], style={'display':'inline-block','margin-left':'8px'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['ปริญญาตรี']], 
                id='STUDY_LEVEL_NAME',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='STUDY_LEVEL_NAME'
            )
        ], style={'display':'inline-block','margin-left':'8px'}),

        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['2559','2560','2561','2562','2563','2564']], 
                id='ปีการศึกษา',
                style={'width': '150px',
                       'marginRight' : '20px'},
                placeholder='ปีการศึกษา'
            )
        ], style={'display':'inline-block','margin-left':'8px'}),

        html.Div([
            html.Br(),
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['ชาย','หญิง']],
                id='เพศ',
                style={'width': '150px',
                    'marginRight' : '20px'} , 
                placeholder='เพศ'
            )
        ], style={'display':'inline-block'}),


        html.Div([
            html.H1(style = {'font-size':'20px',
                                'textAlign' : 'center'}),
            dcc.Dropdown(
                options=[{'label': col, 'value': col} for col in ['Admission','โครงการ "โควตาวิศวกรรมศาสตร์ มอ."','โครงการ "ทุนมงคลสุข"','โครงการ "ลูกพระราชบิดา "','โครงการการคัดเลือกบุคคลเข้าศึกษาระดับปริญญาตรี รอบที่ 3 รับตรงกลางกัน (Admission 2)','โครงการความร่วมมือกับมูลนิธิส่งเสริมโอลิมปิกวิชาการและพัฒนามาตรฐานวิทยาศาสตร์ในพระอุปถัมภ์ (สอวน.)',
                                           'โครงการคัดเลือกนักเรียนเข้าศึกษาในคณะวิศวกรรมศาสตร์ สาขาวิชาวิศวกรรมและการจัดการนวัตกรรม ',
                                           'โครงการคัดเลือกนักเรียนใน 14  จังหวัดภาคใต้ เข้าศึกษาโดยวิธีรับตรง (โควตาภูมิภาค)','โครงการคัดเลือกบุคคลเข้าศึกษาโดยใช้ผลคะแนนสอบ GAT/PAT และวิชาสามัญ 9 วิชา','โครงการคัดเลือกผู้มีความสามารถด้านกีฬา','โครงการดาวรุ่งคอมพิวเตอร์','โครงการรับนักเรียนที่มีผลการเรียนดี',
                                           'โครงการส่งเสริมผู้มีคุณธรรม จริยธรรม บำเพ็ญประโยชน์ช่วยเหลือสังคม','ทุนอุดมศึกษาจังหวัดชายแดนภาคใต้(ศอ.บต.)']], 
                id='โครงการ',
                style={'width': '150px',
                    'marginRight' : '20px'} , 
                placeholder='โครงการ'
            )
        ], style={'display':'inline-block'}),

])
    
html.Div(id='pandas-output-container-1')

    

# @app.callback(
#     Output('pandas-output-container-1', 'children'),
#     Input('MAJOR_ID', 'value'),
#     Input('DEPT_ID', 'value'),
#     Input('STUDY_TYPE_ID','value'),
#     Input('SEX_SHORT_NAME_THAI','value'),
#     Input('ENT_METHOD','value'),
#     Input('FUND_NAME_CODE','value'),
    

# )
# def update_output(value1, value2, value3,value4,value5,value6):
#     return 

input_txt = html.Div(
    [
        html.Br(),
        dcc.Input(id="input1", type="text", 
                  placeholder="เกรดปี1เทอม1", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input2", type="text",
                   placeholder="เกรดปี1เทอม2",
                   style={'marginRight':'20px',
                          'textAlign' : 'center',
                          'font-size' : '15px' }),
        dcc.Input(id="input3", type="text", 
                  placeholder="เกรดปี2เทอม1", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input4", type="text", 
                  placeholder="เกรดปี2เทอม2", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input5", type="text", 
                  placeholder="เกรดปี3เทอม1", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input6", type="text", 
                  placeholder="เกรดปี3เทอม2", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input7", type="text", 
                  placeholder="เกรดปี4เทอม1", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        dcc.Input(id="input8", type="text", 
                  placeholder="เกรดปี4เทอม2", 
                  style={'marginRight':'20px',
                         'textAlign' : 'center',
                         'font-size' : '15px' }),
        # dcc.Input(id="input9", type="text", 
        #           placeholder="GPA", 
        #           style={'marginRight':'20px',
        #                  'textAlign' : 'center',
        #                  'font-size' : '15px' }),
        # dcc.Input(id="input9", type="text", 
        #           placeholder="ENG_SCORE", 
        #           style={'marginRight':'20px',
        #                  'textAlign' : 'center',
        #                  'font-size' : '15px' ,
        #                  'marginTop' : '20px'}),

        html.Button('Prediction',id = 'button',n_clicks= 0),

        html.Div(id="output"),
    ]
)
app.layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(sidebar, width=3, className='bg-light'),
                dbc.Col(
                    [
                        dbc.Row(input_txt),
                        dbc.Row(drop_down),
                        dbc.Row(content),
                    ],
                    width=9,
                ),
            ]
        ),
    ],
    fluid=True,
)


# @app.callback(
#     Output("output","children"),
#     [Input('button','n_clicks')],
#     Input("input1", "value"),
#     Input("input2", "value"),
#     Input("input3","value"),
#     Input("input4","value"),
#     Input("input5","value"),
#     Input("input6","value"),
#     Input("input7","value"),
#     Input("input8","value"),
#     # Input("input8","value"),
#     Input("input9","value")
    

def update_output(input1, input2, input3, input4, input5, input6, input7, input8,input9, input10):
    return



if __name__ == "__main__":
    app.run_server(debug=True, port=1234)
